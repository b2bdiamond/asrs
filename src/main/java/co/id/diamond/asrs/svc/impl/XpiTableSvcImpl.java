package co.id.diamond.asrs.svc.impl;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.id.diamond.asrs.dao.ProTableViewDao;
import co.id.diamond.asrs.dao.XpiTableDao;
import co.id.diamond.asrs.dto.PostDto;
import co.id.diamond.asrs.dto.ProTableDto;
import co.id.diamond.asrs.entity.ProTableView;
import co.id.diamond.asrs.entity.XpiTable;
import co.id.diamond.asrs.entity.XpiTablePk;
import co.id.diamond.asrs.svc.XpiTableSvc;
import ma.glasnost.orika.MapperFacade;

@Service("xpiTableSvc")
@Transactional
public class XpiTableSvcImpl implements XpiTableSvc {

	@Autowired
	XpiTableDao xpiTableDao;
	
	@Autowired
	ProTableViewDao proTableViewDao;
	
	@Autowired
	MapperFacade mapperfacade;
	
	@Override
	public HashMap<String, Object> getData(String tgl, String resource, String status) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		List<ProTableView> listProTable = proTableViewDao.getDataPro(tgl, resource, status);
		List<ProTableDto> listProDto = new ArrayList<ProTableDto>();
		if(listProTable.size()!=0) {
			for (ProTableView proTable : listProTable) {
				ProTableDto dto = mapperfacade.map(proTable, ProTableDto.class);
				dto.setStandartPallet1Qty(proTable.getLeQty1());
				dto.setStandartPallet2Qty(proTable.getLeQty2());
				dto.setUomStandartPallet1(proTable.getUnitOfMeasure1());
				dto.setUomStandartPallet2(proTable.getUnitOfMeasure2());
				dto.setExpirationDate(convertDate(proTable.getExpiredDate()));
				dto.setBasicStartDate(convertDate(proTable.getBasicStartDate()));
				dto.setCreatedOn(convertDate(proTable.getCreatedOn()));
				dto.setLastChange(convertDate(proTable.getLastChange()));
				listProDto.add(dto);
			}
			result.put("status", 1);
			result.put("message", "succes");
			result.put("object", listProDto);
		}else {
			result.put("status", 0);
			result.put("message", "data kosong");
			result.put("object", null);
		}
		
		return result;
	}

	@Override
	public HashMap<String, Object> postData(PostDto dto) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		XpiTable xpiTable = mapperfacade.map(dto, XpiTable.class);
		xpiTable.setExpiredDate(convertDateFromString(dto.getExpDate()));
		xpiTable.setFlag("0");
		
		if(xpiTable.getExpiredDate()==null) {
			result.put("status", 0);
			result.put("message", "failed");
			result.put("object", null);
		}else {
			try {
				xpiTableDao.save(xpiTable);
				result.put("status", 1);
				result.put("message", "succes");
				result.put("object", null);
			}catch (Exception e) {
				result.put("status", 0);
				result.put("message", "failed");
				result.put("object", null);
			}
		}
		
		
		return result;
	}
	
	public String convertDate(Date tgl) {
		Date a = new Date(0);
		try {
		if(tgl.before(a)) {
			return "E";
		}else {
			
				SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
				String dateStr = format.format(tgl);
				return dateStr;
			
		}}catch (Exception e) {
			return "E";
		}
		
		
		
	}
	
	public Date convertDateFromString(String tgl) {
		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
		try {
			Date date = format.parse(tgl);
			return date;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
		
	}

	@Override
	public HashMap<String, Object> getDataXpiTable() {
		HashMap<String, Object> result = new HashMap<String, Object>();
		List<XpiTable> listXpiTable = xpiTableDao.findAll();
		if(listXpiTable.size()!=0) {
			
			result.put("status", 1);
			result.put("message", "succes");
			result.put("object", listXpiTable);
		}else {
			result.put("status", 0);
			result.put("message", "data kosong");
			result.put("object", null);
		}
		
		return result;
	}

	@Override
	public HashMap<String, Object> getOneDataXpiTable(String id) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		XpiTable xpiTable = xpiTableDao.getOneXpiTable(id);;
		if(xpiTable!=null) {
			result.put("status", 1);
			result.put("message", "succes");
			result.put("object", xpiTable);
		}else {
			result.put("status", 0);
			result.put("message", "data kosong");
			result.put("object", null);
		}
		return result;
	}

	@Override
	public HashMap<String, Object> postData(PostDto dto, String id) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		XpiTable xpiTable = new XpiTable();
		if(id.equalsIgnoreCase("")) {
			xpiTable = mapperfacade.map(dto, XpiTable.class);
		}else {
			XpiTablePk pk = new XpiTablePk();
			pk.setBatch(dto.getBatch());
			pk.setPalletNumber(dto.getPalletNumber());
			pk.setMaterialNumber(dto.getMaterialNumber());
			xpiTable = xpiTableDao.getOne(pk);
			xpiTable.setMaterialNumber(dto.getMaterialNumber());
			xpiTable.setQty(new BigDecimal(dto.getQty()));
			xpiTable.setBatch(dto.getBatch());
			xpiTable.setPalletNumber(dto.getPalletNumber());
			xpiTable.setExpiredDate(convertDateFromString(dto.getExpDate()));
			xpiTable.setFlag("0");
			
		}
		try {
			xpiTableDao.save(xpiTable);
			result.put("status", 1);
			result.put("message", "succes");
			result.put("object", null);
		}catch (Exception e) {
			result.put("status", 0);
			result.put("message", "gagal simpan");
			result.put("object", null);
		}
		return result;
	}

	@Override
	public HashMap<String, Object> delOneDataXpiTable(String id) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		try {
			xpiTableDao.deleteXpiTable(id);
			result.put("status", 1);
			result.put("message", "succes");
			result.put("object", null);
		} catch (Exception e) {
			result.put("status", 0);
			result.put("message", "gagal");
			result.put("object", null);
		}
		return result;
	}

	@Override
	public HashMap<String, Object> getLastPalleteNumber() {
		HashMap<String, Object> result = new HashMap<String, Object>();
		try {
			String a = proTableViewDao.getLastPalletNumber();
			result.put("status", 1);
			result.put("message", "succes");
			result.put("object", a);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("status", 0);
			result.put("message", "gagal");
			result.put("object", null);
		}
		return result;
	}

}
