package co.id.diamond.asrs.svc;

import java.util.Date;
import java.util.HashMap;

import co.id.diamond.asrs.dto.PostDto;

public interface XpiTableSvc {

	public HashMap<String, Object> getData(String tgl,String resource,String status);
	public HashMap<String, Object> postData(PostDto dto);
	public HashMap<String, Object> getDataXpiTable();
	public HashMap<String, Object> getOneDataXpiTable(String id);
	public HashMap<String, Object> postData(PostDto dto,String id);
	public HashMap<String, Object> delOneDataXpiTable(String id);
	public HashMap<String, Object> getLastPalleteNumber();
	
}
