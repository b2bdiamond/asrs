package co.id.diamond.asrs.ctl;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import co.id.diamond.asrs.config.RestResponse;
import co.id.diamond.asrs.dto.PostDto;
import co.id.diamond.asrs.svc.XpiTableSvc;

@RestController
public class ProTableCtl {

	@Autowired
	XpiTableSvc xpiTableSvc;
	
	@RequestMapping(value = "/getProData", method = RequestMethod.GET)
	public ResponseEntity<RestResponse> getData(@RequestParam(required=false,defaultValue = "") String date, 
			@RequestParam(required=false,defaultValue = "") String resource,
			@RequestParam(required=false,defaultValue = "") String status) {
		HashMap<String, Object> result = xpiTableSvc.getData(date, resource, status);
		RestResponse response = new RestResponse((int) result.get("status"), (String) result.get("message"),
				result.get("object"));
		ResponseEntity<RestResponse> responseEntity = new ResponseEntity<>(response, HttpStatus.OK);
		return responseEntity;
	}
	
	@RequestMapping(value = "/postProData", method = RequestMethod.POST)
	public ResponseEntity<RestResponse> postData(@RequestBody PostDto dto) {
		HashMap<String, Object> result = xpiTableSvc.postData(dto);
		RestResponse response = new RestResponse((int) result.get("status"), (String) result.get("message"),
				result.get("object"));
		ResponseEntity<RestResponse> responseEntity = new ResponseEntity<>(response, HttpStatus.OK);
		return responseEntity;
	}
	
	@RequestMapping(value = "/postXpiData", method = RequestMethod.POST)
	public ResponseEntity<RestResponse> postXpiData(@RequestBody PostDto dto,
			@RequestParam(required=false,defaultValue = "") String id) {
		HashMap<String, Object> result = xpiTableSvc.postData(dto, id);
		RestResponse response = new RestResponse((int) result.get("status"), (String) result.get("message"),
				result.get("object"));
		ResponseEntity<RestResponse> responseEntity = new ResponseEntity<>(response, HttpStatus.OK);
		return responseEntity;
	}
	
	@RequestMapping(value = "/getDataXpi", method = RequestMethod.GET)
	public ResponseEntity<RestResponse> getDataXpi() {
		HashMap<String, Object> result = xpiTableSvc.getDataXpiTable();
		RestResponse response = new RestResponse((int) result.get("status"), (String) result.get("message"),
				result.get("object"));
		ResponseEntity<RestResponse> responseEntity = new ResponseEntity<>(response, HttpStatus.OK);
		return responseEntity;
	}
	
	@RequestMapping(value = "/getOneDataXpi", method = RequestMethod.GET)
	public ResponseEntity<RestResponse> getOneDataXpi(@RequestParam(required=false,defaultValue = "") String id) {
		HashMap<String, Object> result = xpiTableSvc.getOneDataXpiTable(id);
		RestResponse response = new RestResponse((int) result.get("status"), (String) result.get("message"),
				result.get("object"));
		ResponseEntity<RestResponse> responseEntity = new ResponseEntity<>(response, HttpStatus.OK);
		return responseEntity;
	}
	
	@RequestMapping(value = "/getLastPalletTable", method = RequestMethod.GET)
	public ResponseEntity<RestResponse> getLastPalletTable() {
		HashMap<String, Object> result = xpiTableSvc.getLastPalleteNumber();
		RestResponse response = new RestResponse((int) result.get("status"), (String) result.get("message"),
				result.get("object"));
		ResponseEntity<RestResponse> responseEntity = new ResponseEntity<>(response, HttpStatus.OK);
		return responseEntity;
	}
}
