package co.id.diamond.asrs.dto;

public class PostDto {

	private String proNumber;
	private String materialNumber;
	private Integer qty;
	private String batch;
	private String palletNumber;
	private String expDate;
	private String itf;
	private Integer qtyPc;
	
	public String getItf() {
		return itf;
	}
	public void setItf(String itf) {
		this.itf = itf;
	}
	public Integer getQtyPc() {
		return qtyPc;
	}
	public void setQtyPc(Integer qtyPc) {
		this.qtyPc = qtyPc;
	}
	public String getProNumber() {
		return proNumber;
	}
	public void setProNumber(String proNumber) {
		this.proNumber = proNumber;
	}
	public String getMaterialNumber() {
		return materialNumber;
	}
	public void setMaterialNumber(String materialNumber) {
		this.materialNumber = materialNumber;
	}
	public Integer getQty() {
		return qty;
	}
	public void setQty(Integer qty) {
		this.qty = qty;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public String getPalletNumber() {
		return palletNumber;
	}
	public void setPalletNumber(String palletNumber) {
		this.palletNumber = palletNumber;
	}
	public String getExpDate() {
		return expDate;
	}
	public void setExpDate(String expDate) {
		this.expDate = expDate;
	}

}
