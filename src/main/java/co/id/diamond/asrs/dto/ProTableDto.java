package co.id.diamond.asrs.dto;


import javax.persistence.Column;
import javax.persistence.Id;

public class ProTableDto {

	private String baseUom;

	private String basicStartDate;

	private String batch;

	private String businessModel;

	private String category1;

	private String category2;

	private String changedBy;

	private String createdBy;

	private String createdOn;

	private String delivPlant;

	private String dfClientLevel;

	private String distributionChannel;

	private String division;

	private String expirationDate;

	private String fiCat;

	private String fiSSubC;

	private String fiSubCat;

	private Integer grossWeight;

	private Integer height;

	private String lastChange;

	private Integer standartPallet1Qty;

	private Integer standartPallet2Qty;

	private Integer length;

	private String materialDescription;

	private String materialGroup;

	private String materialNumber;

	private String materialType;

	private String mkt1;

	private String mkt2;

	private String mkt3;

	private Integer netWeight;

	private String oldMatlNumber;

	private String orderUnit;

	private String packSizeOld;

	private String periodInd;

	private String proOrder;

	private String prodHierarchy;

	private String productHierarchyLvl3Code;

	private String purchValueKey;

	private Integer qtyPro;

	private String remShelfLife;

	private String resourcePro;

	private String salesUnit;

	private String sizeDimension;

	private String status;

	private String storageCondition;

	private String temperature;

	private String totShelfLife;

	private String transportationGroup;

	private String unitDimension;

	private String uomStandartPallet1;

	private String uomStandartPallet2;

	private Integer volume;

	private String volumeUnit;

	private String weightUnit;

	private Integer width;
	private String ITF; 

	public String getBaseUom() {
		return baseUom;
	}

	public String getITF() {
		return ITF;
	}

	public void setITF(String iTF) {
		ITF = iTF;
	}

	public void setBaseUom(String baseUom) {
		this.baseUom = baseUom;
	}

	public String getBasicStartDate() {
		return basicStartDate;
	}

	public void setBasicStartDate(String basicStartDate) {
		this.basicStartDate = basicStartDate;
	}

	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	public String getBusinessModel() {
		return businessModel;
	}

	public void setBusinessModel(String businessModel) {
		this.businessModel = businessModel;
	}

	public String getCategory1() {
		return category1;
	}

	public void setCategory1(String category1) {
		this.category1 = category1;
	}

	public String getCategory2() {
		return category2;
	}

	public void setCategory2(String category2) {
		this.category2 = category2;
	}

	public String getChangedBy() {
		return changedBy;
	}

	public void setChangedBy(String changedBy) {
		this.changedBy = changedBy;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public String getDelivPlant() {
		return delivPlant;
	}

	public void setDelivPlant(String delivPlant) {
		this.delivPlant = delivPlant;
	}

	public String getDfClientLevel() {
		return dfClientLevel;
	}

	public void setDfClientLevel(String dfClientLevel) {
		this.dfClientLevel = dfClientLevel;
	}

	public String getDistributionChannel() {
		return distributionChannel;
	}

	public void setDistributionChannel(String distributionChannel) {
		this.distributionChannel = distributionChannel;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getFiCat() {
		return fiCat;
	}

	public void setFiCat(String fiCat) {
		this.fiCat = fiCat;
	}

	public String getFiSSubC() {
		return fiSSubC;
	}

	public void setFiSSubC(String fiSSubC) {
		this.fiSSubC = fiSSubC;
	}

	public String getFiSubCat() {
		return fiSubCat;
	}

	public void setFiSubCat(String fiSubCat) {
		this.fiSubCat = fiSubCat;
	}

	public Integer getGrossWeight() {
		return grossWeight;
	}

	public void setGrossWeight(Integer grossWeight) {
		this.grossWeight = grossWeight;
	}

	public Integer getHeight() {
		return height;
	}

	public void setHeight(Integer height) {
		this.height = height;
	}

	public String getLastChange() {
		return lastChange;
	}

	public void setLastChange(String lastChange) {
		this.lastChange = lastChange;
	}

	


	public Integer getStandartPallet1Qty() {
		return standartPallet1Qty;
	}

	public void setStandartPallet1Qty(Integer standartPallet1Qty) {
		this.standartPallet1Qty = standartPallet1Qty;
	}



	public Integer getStandartPallet2Qty() {
		return standartPallet2Qty;
	}

	public void setStandartPallet2Qty(Integer standartPallet2Qty) {
		this.standartPallet2Qty = standartPallet2Qty;
	}

	public Integer getLength() {
		return length;
	}

	public void setLength(Integer length) {
		this.length = length;
	}

	public String getMaterialDescription() {
		return materialDescription;
	}

	public void setMaterialDescription(String materialDescription) {
		this.materialDescription = materialDescription;
	}

	public String getMaterialGroup() {
		return materialGroup;
	}

	public void setMaterialGroup(String materialGroup) {
		this.materialGroup = materialGroup;
	}

	public String getMaterialNumber() {
		return materialNumber;
	}

	public void setMaterialNumber(String materialNumber) {
		this.materialNumber = materialNumber;
	}

	public String getMaterialType() {
		return materialType;
	}

	public void setMaterialType(String materialType) {
		this.materialType = materialType;
	}

	public String getMkt1() {
		return mkt1;
	}

	public void setMkt1(String mkt1) {
		this.mkt1 = mkt1;
	}

	public String getMkt2() {
		return mkt2;
	}

	public void setMkt2(String mkt2) {
		this.mkt2 = mkt2;
	}

	public String getMkt3() {
		return mkt3;
	}

	public void setMkt3(String mkt3) {
		this.mkt3 = mkt3;
	}

	public Integer getNetWeight() {
		return netWeight;
	}

	public void setNetWeight(Integer netWeight) {
		this.netWeight = netWeight;
	}

	public String getOldMatlNumber() {
		return oldMatlNumber;
	}

	public void setOldMatlNumber(String oldMatlNumber) {
		this.oldMatlNumber = oldMatlNumber;
	}

	public String getOrderUnit() {
		return orderUnit;
	}

	public void setOrderUnit(String orderUnit) {
		this.orderUnit = orderUnit;
	}

	public String getPackSizeOld() {
		return packSizeOld;
	}

	public void setPackSizeOld(String packSizeOld) {
		this.packSizeOld = packSizeOld;
	}

	public String getPeriodInd() {
		return periodInd;
	}

	public void setPeriodInd(String periodInd) {
		this.periodInd = periodInd;
	}

	public String getProOrder() {
		return proOrder;
	}

	public void setProOrder(String proOrder) {
		this.proOrder = proOrder;
	}

	public String getProdHierarchy() {
		return prodHierarchy;
	}

	public void setProdHierarchy(String prodHierarchy) {
		this.prodHierarchy = prodHierarchy;
	}

	public String getProductHierarchyLvl3Code() {
		return productHierarchyLvl3Code;
	}

	public void setProductHierarchyLvl3Code(String productHierarchyLvl3Code) {
		this.productHierarchyLvl3Code = productHierarchyLvl3Code;
	}

	public String getPurchValueKey() {
		return purchValueKey;
	}

	public void setPurchValueKey(String purchValueKey) {
		this.purchValueKey = purchValueKey;
	}

	public Integer getQtyPro() {
		return qtyPro;
	}

	public void setQtyPro(Integer qtyPro) {
		this.qtyPro = qtyPro;
	}

	public String getRemShelfLife() {
		return remShelfLife;
	}

	public void setRemShelfLife(String remShelfLife) {
		this.remShelfLife = remShelfLife;
	}

	public String getResourcePro() {
		return resourcePro;
	}

	public void setResourcePro(String resourcePro) {
		this.resourcePro = resourcePro;
	}

	public String getSalesUnit() {
		return salesUnit;
	}

	public void setSalesUnit(String salesUnit) {
		this.salesUnit = salesUnit;
	}

	public String getSizeDimension() {
		return sizeDimension;
	}

	public void setSizeDimension(String sizeDimension) {
		this.sizeDimension = sizeDimension;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStorageCondition() {
		return storageCondition;
	}

	public void setStorageCondition(String storageCondition) {
		this.storageCondition = storageCondition;
	}

	public String getTemperature() {
		return temperature;
	}

	public void setTemperature(String temperature) {
		this.temperature = temperature;
	}

	public String getTotShelfLife() {
		return totShelfLife;
	}

	public void setTotShelfLife(String totShelfLife) {
		this.totShelfLife = totShelfLife;
	}

	public String getTransportationGroup() {
		return transportationGroup;
	}

	public void setTransportationGroup(String transportationGroup) {
		this.transportationGroup = transportationGroup;
	}

	public String getUnitDimension() {
		return unitDimension;
	}

	public void setUnitDimension(String unitDimension) {
		this.unitDimension = unitDimension;
	}


	public String getUomStandartPallet1() {
		return uomStandartPallet1;
	}

	public void setUomStandartPallet1(String uomStandartPallet1) {
		this.uomStandartPallet1 = uomStandartPallet1;
	}

	public String getUomStandartPallet2() {
		return uomStandartPallet2;
	}

	public void setUomStandartPallet2(String uomStandartPallet2) {
		this.uomStandartPallet2 = uomStandartPallet2;
	}

	public Integer getVolume() {
		return volume;
	}

	public void setVolume(Integer volume) {
		this.volume = volume;
	}

	public String getVolumeUnit() {
		return volumeUnit;
	}

	public void setVolumeUnit(String volumeUnit) {
		this.volumeUnit = volumeUnit;
	}

	public String getWeightUnit() {
		return weightUnit;
	}

	public void setWeightUnit(String weightUnit) {
		this.weightUnit = weightUnit;
	}

	public Integer getWidth() {
		return width;
	}

	public void setWidth(Integer width) {
		this.width = width;
	}
	
	
}
