package co.id.diamond.asrs.entity;

import java.io.Serializable;
import javax.persistence.*;


import java.util.Date;


/**
 * The persistent class for the PRO_TABLE_VIEW database table.
 * 
 */
@Entity
@Table(name="PRO_TABLE_VIEW")
@IdClass(ProTableViewPK.class)
@NamedQuery(name="ProTableView.findAll", query="SELECT p FROM ProTableView p")
public class ProTableView implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="BASE_UOM")
	private String baseUom;

	@Id
	@Temporal(TemporalType.DATE)
	@Column(name="BASIC_START_DATE")
	private Date basicStartDate;

	private String batch;

	@Column(name="BUSINESS_MODEL")
	private String businessModel;

	@Column(name="CATEGORY_1")
	private String category1;

	@Column(name="CATEGORY_2")
	private String category2;

	@Column(name="CHANGED_BY")
	private String changedBy;

	@Column(name="CREATED_BY")
	private String createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_ON")
	private Date createdOn;

	@Column(name="DELIV_PLANT")
	private String delivPlant;

	@Column(name="DF_CLIENT_LEVEL")
	private String dfClientLevel;

	@Column(name="DISTRIBUTION_CHANNEL")
	private String distributionChannel;

	private String division;

	@Temporal(TemporalType.DATE)
	@Column(name="EXPIRED_DATE")
	private Date expiredDate;

	@Column(name="FI_CAT")
	private String fiCat;

	@Column(name="FI_S_SUB_C")
	private String fiSSubC;

	@Column(name="FI_SUB_CAT")
	private String fiSubCat;

	@Column(name="GROSS_WEIGHT")
	private Integer grossWeight;

	private Integer height;
	private String ITF; 

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_CHANGE")
	private Date lastChange;

	@Column(name="LE_QTY_1")
	private Integer leQty1;

	@Column(name="LE_QTY_2")
	private Integer leQty2;

	private Integer length;

	@Column(name="MATERIAL_DESCRIPTION")
	private String materialDescription;

	@Column(name="MATERIAL_GROUP")
	private String materialGroup;

	@Column(name="MATERIAL_NUMBER")
	private String materialNumber;

	@Column(name="MATERIAL_TYPE")
	private String materialType;

	@Column(name="MKT_1")
	private String mkt1;

	@Column(name="MKT_2")
	private String mkt2;

	@Column(name="MKT_3")
	private String mkt3;

	@Column(name="NET_WEIGHT")
	private Integer netWeight;

	@Column(name="OLD_MATL_NUMBER")
	private String oldMatlNumber;

	@Column(name="ORDER_UNIT")
	private String orderUnit;

	@Column(name="PACK_SIZE_OLD")
	private String packSizeOld;

	@Column(name="PERIOD_IND")
	private String periodInd;

	@Id
	@Column(name="PRO_ORDER")
	private String proOrder;

	@Column(name="PROD_HIERARCHY")
	private String prodHierarchy;

	@Column(name="PRODUCT_HIERARCHY_LVL_3_CODE")
	private String productHierarchyLvl3Code;

	@Column(name="PURCH_VALUE_KEY")
	private String purchValueKey;

	@Column(name="QTY_PRO")
	private Integer qtyPro;

	@Column(name="REM_SHELF_LIFE")
	private String remShelfLife;

	@Column(name="RESOURCE_PRO")
	private String resourcePro;

	@Column(name="SALES_UNIT")
	private String salesUnit;

	@Column(name="SIZE_DIMENSION")
	private String sizeDimension;

	private String status;

	@Column(name="STORAGE_CONDITION")
	private String storageCondition;

	private String temperature;

	@Column(name="TOT_SHELF_LIFE")
	private String totShelfLife;

	@Column(name="TRANSPORTATION_GROUP")
	private String transportationGroup;

	@Column(name="UNIT_DIMENSION")
	private String unitDimension;

	@Column(name="UNIT_OF_MEASURE_1")
	private String unitOfMeasure1;

	@Column(name="UNIT_OF_MEASURE_2")
	private String unitOfMeasure2;

	private Integer volume;

	@Column(name="VOLUME_UNIT")
	private String volumeUnit;

	@Column(name="WEIGHT_UNIT")
	private String weightUnit;

	private Integer width;

	public ProTableView() {
	}

	public String getITF() {
		return ITF;
	}

	public void setITF(String iTF) {
		ITF = iTF;
	}

	public String getBaseUom() {
		return this.baseUom;
	}

	public void setBaseUom(String baseUom) {
		this.baseUom = baseUom;
	}

	public Date getBasicStartDate() {
		return this.basicStartDate;
	}

	public void setBasicStartDate(Date basicStartDate) {
		this.basicStartDate = basicStartDate;
	}

	public String getBatch() {
		return this.batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	public String getBusinessModel() {
		return this.businessModel;
	}

	public void setBusinessModel(String businessModel) {
		this.businessModel = businessModel;
	}

	public String getCategory1() {
		return this.category1;
	}

	public void setCategory1(String category1) {
		this.category1 = category1;
	}

	public String getCategory2() {
		return this.category2;
	}

	public void setCategory2(String category2) {
		this.category2 = category2;
	}

	public String getChangedBy() {
		return this.changedBy;
	}

	public void setChangedBy(String changedBy) {
		this.changedBy = changedBy;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getDelivPlant() {
		return this.delivPlant;
	}

	public void setDelivPlant(String delivPlant) {
		this.delivPlant = delivPlant;
	}

	public String getDfClientLevel() {
		return this.dfClientLevel;
	}

	public void setDfClientLevel(String dfClientLevel) {
		this.dfClientLevel = dfClientLevel;
	}

	public String getDistributionChannel() {
		return this.distributionChannel;
	}

	public void setDistributionChannel(String distributionChannel) {
		this.distributionChannel = distributionChannel;
	}

	public String getDivision() {
		return this.division;
	}

	public void setDivision(String division) {
		this.division = division;
	}



	public Date getExpiredDate() {
		return expiredDate;
	}

	public void setExpiredDate(Date expiredDate) {
		this.expiredDate = expiredDate;
	}

	public String getFiCat() {
		return this.fiCat;
	}

	public void setFiCat(String fiCat) {
		this.fiCat = fiCat;
	}

	public String getFiSSubC() {
		return this.fiSSubC;
	}

	public void setFiSSubC(String fiSSubC) {
		this.fiSSubC = fiSSubC;
	}

	public String getFiSubCat() {
		return this.fiSubCat;
	}

	public void setFiSubCat(String fiSubCat) {
		this.fiSubCat = fiSubCat;
	}

	public Integer getGrossWeight() {
		return this.grossWeight;
	}

	public void setGrossWeight(Integer grossWeight) {
		this.grossWeight = grossWeight;
	}

	public Integer getHeight() {
		return this.height;
	}

	public void setHeight(Integer height) {
		this.height = height;
	}

	public Date getLastChange() {
		return this.lastChange;
	}

	public void setLastChange(Date lastChange) {
		this.lastChange = lastChange;
	}

	public Integer getLeQty1() {
		return this.leQty1;
	}

	public void setLeQty1(Integer leQty1) {
		this.leQty1 = leQty1;
	}

	public Integer getLeQty2() {
		return this.leQty2;
	}

	public void setLeQty2(Integer leQty2) {
		this.leQty2 = leQty2;
	}

	public Integer getLength() {
		return this.length;
	}

	public void setLength(Integer length) {
		this.length = length;
	}

	public String getMaterialDescription() {
		return this.materialDescription;
	}

	public void setMaterialDescription(String materialDescription) {
		this.materialDescription = materialDescription;
	}

	public String getMaterialGroup() {
		return this.materialGroup;
	}

	public void setMaterialGroup(String materialGroup) {
		this.materialGroup = materialGroup;
	}

	public String getMaterialNumber() {
		return this.materialNumber;
	}

	public void setMaterialNumber(String materialNumber) {
		this.materialNumber = materialNumber;
	}

	public String getMaterialType() {
		return this.materialType;
	}

	public void setMaterialType(String materialType) {
		this.materialType = materialType;
	}

	public String getMkt1() {
		return this.mkt1;
	}

	public void setMkt1(String mkt1) {
		this.mkt1 = mkt1;
	}

	public String getMkt2() {
		return this.mkt2;
	}

	public void setMkt2(String mkt2) {
		this.mkt2 = mkt2;
	}

	public String getMkt3() {
		return this.mkt3;
	}

	public void setMkt3(String mkt3) {
		this.mkt3 = mkt3;
	}

	public Integer getNetWeight() {
		return this.netWeight;
	}

	public void setNetWeight(Integer netWeight) {
		this.netWeight = netWeight;
	}

	public String getOldMatlNumber() {
		return this.oldMatlNumber;
	}

	public void setOldMatlNumber(String oldMatlNumber) {
		this.oldMatlNumber = oldMatlNumber;
	}

	public String getOrderUnit() {
		return this.orderUnit;
	}

	public void setOrderUnit(String orderUnit) {
		this.orderUnit = orderUnit;
	}

	public String getPackSizeOld() {
		return this.packSizeOld;
	}

	public void setPackSizeOld(String packSizeOld) {
		this.packSizeOld = packSizeOld;
	}

	public String getPeriodInd() {
		return this.periodInd;
	}

	public void setPeriodInd(String periodInd) {
		this.periodInd = periodInd;
	}

	public String getProOrder() {
		return this.proOrder;
	}

	public void setProOrder(String proOrder) {
		this.proOrder = proOrder;
	}

	public String getProdHierarchy() {
		return this.prodHierarchy;
	}

	public void setProdHierarchy(String prodHierarchy) {
		this.prodHierarchy = prodHierarchy;
	}

	public String getProductHierarchyLvl3Code() {
		return this.productHierarchyLvl3Code;
	}

	public void setProductHierarchyLvl3Code(String productHierarchyLvl3Code) {
		this.productHierarchyLvl3Code = productHierarchyLvl3Code;
	}

	public String getPurchValueKey() {
		return this.purchValueKey;
	}

	public void setPurchValueKey(String purchValueKey) {
		this.purchValueKey = purchValueKey;
	}

	public Integer getQtyPro() {
		return this.qtyPro;
	}

	public void setQtyPro(Integer qtyPro) {
		this.qtyPro = qtyPro;
	}

	public String getRemShelfLife() {
		return this.remShelfLife;
	}

	public void setRemShelfLife(String remShelfLife) {
		this.remShelfLife = remShelfLife;
	}

	public String getResourcePro() {
		return this.resourcePro;
	}

	public void setResourcePro(String resourcePro) {
		this.resourcePro = resourcePro;
	}

	public String getSalesUnit() {
		return this.salesUnit;
	}

	public void setSalesUnit(String salesUnit) {
		this.salesUnit = salesUnit;
	}

	public String getSizeDimension() {
		return this.sizeDimension;
	}

	public void setSizeDimension(String sizeDimension) {
		this.sizeDimension = sizeDimension;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStorageCondition() {
		return this.storageCondition;
	}

	public void setStorageCondition(String storageCondition) {
		this.storageCondition = storageCondition;
	}

	public String getTemperature() {
		return this.temperature;
	}

	public void setTemperature(String temperature) {
		this.temperature = temperature;
	}

	public String getTotShelfLife() {
		return this.totShelfLife;
	}

	public void setTotShelfLife(String totShelfLife) {
		this.totShelfLife = totShelfLife;
	}

	public String getTransportationGroup() {
		return this.transportationGroup;
	}

	public void setTransportationGroup(String transportationGroup) {
		this.transportationGroup = transportationGroup;
	}

	public String getUnitDimension() {
		return this.unitDimension;
	}

	public void setUnitDimension(String unitDimension) {
		this.unitDimension = unitDimension;
	}

	public String getUnitOfMeasure1() {
		return this.unitOfMeasure1;
	}

	public void setUnitOfMeasure1(String unitOfMeasure1) {
		this.unitOfMeasure1 = unitOfMeasure1;
	}

	public String getUnitOfMeasure2() {
		return this.unitOfMeasure2;
	}

	public void setUnitOfMeasure2(String unitOfMeasure2) {
		this.unitOfMeasure2 = unitOfMeasure2;
	}

	public Integer getVolume() {
		return this.volume;
	}

	public void setVolume(Integer volume) {
		this.volume = volume;
	}

	public String getVolumeUnit() {
		return this.volumeUnit;
	}

	public void setVolumeUnit(String volumeUnit) {
		this.volumeUnit = volumeUnit;
	}

	public String getWeightUnit() {
		return this.weightUnit;
	}

	public void setWeightUnit(String weightUnit) {
		this.weightUnit = weightUnit;
	}

	public Integer getWidth() {
		return this.width;
	}

	public void setWidth(Integer width) {
		this.width = width;
	}

}