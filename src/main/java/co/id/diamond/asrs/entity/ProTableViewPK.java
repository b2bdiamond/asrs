package co.id.diamond.asrs.entity;

import java.io.Serializable;
import java.util.Date;

public class ProTableViewPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4535129759676318698L;
	private Date basicStartDate;
	private String proOrder;
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((basicStartDate == null) ? 0 : basicStartDate.hashCode());
		result = prime * result + ((proOrder == null) ? 0 : proOrder.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProTableViewPK other = (ProTableViewPK) obj;
		if (basicStartDate == null) {
			if (other.basicStartDate != null)
				return false;
		} else if (!basicStartDate.equals(other.basicStartDate))
			return false;
		if (proOrder == null) {
			if (other.proOrder != null)
				return false;
		} else if (!proOrder.equals(other.proOrder))
			return false;
		return true;
	}
	public Date getBasicStartDate() {
		return basicStartDate;
	}
	public void setBasicStartDate(Date basicStartDate) {
		this.basicStartDate = basicStartDate;
	}
	public String getProOrder() {
		return proOrder;
	}
	public void setProOrder(String proOrder) {
		this.proOrder = proOrder;
	}
	
	
}
