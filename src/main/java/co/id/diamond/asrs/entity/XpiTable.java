package co.id.diamond.asrs.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the XPI_TABLE database table.
 * 
 */
@Entity
@IdClass(XpiTablePk.class)
@Table(name="XPI_TABLE")
@NamedQuery(name="XpiTable.findAll", query="SELECT x FROM XpiTable x")
public class XpiTable implements Serializable {
	private static final long serialVersionUID = 1L;

	
	@Column(name="PRO_NUMBER")
	private String proNumber;

	@Id
	private String batch;

	@Column(name="EXPIRED_DATE")
	private Date expiredDate;

	private String flag;

	@Id
	@Column(name="MATERIAL_NUMBER")
	private String materialNumber;

	@Id
	@Column(name="PALLET_NUMBER")
	private String palletNumber;

	private BigDecimal qty;
	
	private String Itf;
	
	@Column(name="QTY_PC")
	private BigDecimal qtyPc;

	public String getItf() {
		return Itf;
	}

	public void setItf(String itf) {
		Itf = itf;
	}

	public BigDecimal getQtyPc() {
		return qtyPc;
	}

	public void setQtyPc(BigDecimal qtyPc) {
		this.qtyPc = qtyPc;
	}

	public XpiTable() {
	}

	public String getProNumber() {
		return this.proNumber;
	}

	public void setProNumber(String proNumber) {
		this.proNumber = proNumber;
	}

	public String getBatch() {
		return this.batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	public Date getExpiredDate() {
		return this.expiredDate;
	}

	public void setExpiredDate(Date expiredDate) {
		this.expiredDate = expiredDate;
	}

	public String getFlag() {
		return this.flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getMaterialNumber() {
		return this.materialNumber;
	}

	public void setMaterialNumber(String materialNumber) {
		this.materialNumber = materialNumber;
	}

	public String getPalletNumber() {
		return this.palletNumber;
	}

	public void setPalletNumber(String palletNumber) {
		this.palletNumber = palletNumber;
	}

	public BigDecimal getQty() {
		return this.qty;
	}

	public void setQty(BigDecimal qty) {
		this.qty = qty;
	}

}