package co.id.diamond.asrs.entity;

import java.io.Serializable;

public class XpiTablePk implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6244944084591692623L;
	
	private String batch;
	private String materialNumber;
	private String palletNumber;
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((batch == null) ? 0 : batch.hashCode());
		result = prime * result + ((materialNumber == null) ? 0 : materialNumber.hashCode());
		result = prime * result + ((palletNumber == null) ? 0 : palletNumber.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		XpiTablePk other = (XpiTablePk) obj;
		if (batch == null) {
			if (other.batch != null)
				return false;
		} else if (!batch.equals(other.batch))
			return false;
		if (materialNumber == null) {
			if (other.materialNumber != null)
				return false;
		} else if (!materialNumber.equals(other.materialNumber))
			return false;
		if (palletNumber == null) {
			if (other.palletNumber != null)
				return false;
		} else if (!palletNumber.equals(other.palletNumber))
			return false;
		return true;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public String getMaterialNumber() {
		return materialNumber;
	}
	public void setMaterialNumber(String materialNumber) {
		this.materialNumber = materialNumber;
	}
	public String getPalletNumber() {
		return palletNumber;
	}
	public void setPalletNumber(String palletNumber) {
		this.palletNumber = palletNumber;
	}
	
	

}
