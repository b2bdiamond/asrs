package co.id.diamond.asrs.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter{
	
//	@Autowired
//	UserDetailsService 
	
	@Autowired
	Environment env;
	
	
    @Override
    protected void configure(HttpSecurity http) throws Exception 
    {
    	final String[] AUTH_WHITELIST = {
    			"/v2/api-docs",
	            "/swagger-resources",
	            "/swagger-resources/**",
	            "/configuration/ui",
	            "/asrs/swagger-ui.html",
	            "/swagger-ui.html",
	            "/"
	
    	};
        http
         .csrf().disable()
         .authorizeRequests()
         .antMatchers(AUTH_WHITELIST).permitAll()
         .anyRequest().authenticated()
         .and()
         .httpBasic();
    }
  
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) 
            throws Exception 
    {
        auth.inMemoryAuthentication()
            .withUser("asrs")
            .password("{noop}vaahf0cmFU")
            .roles("USER");
    }
}
