package co.id.diamond.asrs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AsrsApplication {

	public static void main(String[] args) {
		SpringApplication.run(AsrsApplication.class, args);
	}

}
