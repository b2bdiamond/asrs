package co.id.diamond.asrs.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import co.id.diamond.asrs.entity.XpiTable;
import co.id.diamond.asrs.entity.XpiTablePk;

public interface XpiTableDao extends JpaRepository<XpiTable, XpiTablePk> {

	@Modifying
	@Query(nativeQuery = true, value = "delete from xpi_table where pro_number=:id")
	public void deleteXpiTable(@Param("id") String id);
	
	@Query(nativeQuery = true, value = "select * from xpi_table where pro_number=:id")
	public XpiTable getOneXpiTable(@Param("id") String id);
}
