package co.id.diamond.asrs.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;

import co.id.diamond.asrs.entity.ProTableView;
import co.id.diamond.asrs.entity.ProTableViewPK;

public interface ProTableViewDao extends JpaRepository<ProTableView, ProTableViewPK> {

	@Query(nativeQuery = true, value = "select * from pro_table_view "
			+ "where TO_CHAR(basic_start_date,'DD-MM-YYYY') like CONCAT(CONCAT('%',:date),'%') "
			+ "and resource_pro like CONCAT(CONCAT('%',:resource),'%') "
			+ "and status like CONCAT(CONCAT('%',:status),'%')")
	public List<ProTableView> getDataPro(@Param("date")String date,@Param("resource")String resource,
			@Param("status")String status);
	
	
	@Procedure(name = "GETLASTPALLETNUMBER")
	public String getLastPalletNumber();
}
